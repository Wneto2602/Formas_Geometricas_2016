#ifndef FORMASGEOMETRICAS_H
#define FORMASGEOMETRICAS_H

class FormaGeometrica {
	// Atributos
	private:
		int lados;
		float altura;
		float largura;
		float area;
		float perimetro;
	public:
		// Contrutores
		FormaGeometrica(); // Colocar em Private para criar a classe abstrata
		FormaGeometrica(float altura, float largura);
		//FormaGeometrica(float altura, float largura, int lados);
		// Acessores
		void setAltura(float altura);
		float getAltura();
		void setLargura(float largura);
		float getLargura();
		void setLados(int lados);
		int getLados();
		float getArea();
		float getPerimetro();

		float calculaArea();
		float calculaPerimetro();

	protected:
		void setArea(float area);
		void setPerimetro(float perimetro);
};

#endif